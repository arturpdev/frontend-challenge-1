import api from '../services/pokeApi'

export const getPokemonByNumber = (number: number | string) => api.get(`pokemon/${number}`)