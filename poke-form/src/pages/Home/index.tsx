import { useState, FormEvent } from 'react';
import './styles.css';

import { getPokemonByNumber } from '../../repositories/pokemon';

interface IPicture {
  name: string;
  src: string;
}

interface IPokemonInfos {
  name: string;
  pictures: IPicture[];
};

interface IPokemonInfosResponse {
  name: string;
  sprites: {
    back_default: string | null;
    back_female: string | null;
    back_shiny: string | null;
    back_shiny_female: string | null;
    front_default: string | null;
    front_female: string | null;
    front_shiny: string | null;
    front_shiny_female: string | null;
  };
};

function App() {
  const [pokeNumber, setPokeNumber] = useState('');
  const [pokemon, setPokemon] = useState<IPokemonInfos>({} as IPokemonInfos);
  const [message, setMessage] = useState('Search for pokemon with a number :)');

  const getPokemon = async (event: FormEvent) => {
    event.preventDefault();

    try {
      setPokemon({} as IPokemonInfos);
      setMessage('Loading...');
      const { data } = await getPokemonByNumber(pokeNumber);
      const myPokemon: IPokemonInfos = proccessData(data);
      setPokemon(myPokemon);
      setMessage('Found pokemon :D');
    } catch (err) {
      let error = '';
      if (err.response?.status === 404) {
        error = 'Pokemon not found. Try another number!';
      } else {
        error = 'An error occurred while searching the pokemon :(';
      };

      setMessage(error);
    };
  };

  const proccessData = (pokemonInfoResponse: IPokemonInfosResponse): IPokemonInfos => {
    const { name, sprites } = pokemonInfoResponse;

    const pictures: IPicture[] = [];

    const {
      back_default,
      back_female,
      back_shiny,
      back_shiny_female,
      front_default,
      front_female,
      front_shiny,
      front_shiny_female,
    } = sprites;

    if (!!back_default) pictures.push({ name: 'back_default', src: back_default });
    if (!!back_female) pictures.push({ name: 'back_female', src: back_female });
    if (!!back_shiny) pictures.push({ name: 'back_shiny', src: back_shiny });
    if (!!back_shiny_female) pictures.push({ name: 'back_shiny_female', src: back_shiny_female });
    if (!!front_default) pictures.push({ name: 'front_default', src: front_default });
    if (!!front_female) pictures.push({ name: 'front_female', src: front_female });
    if (!!front_shiny) pictures.push({ name: 'front_shiny', src: front_shiny });
    if (!!front_shiny_female) pictures.push({ name: 'front_shiny_female', src: front_shiny_female });

    return {
      name,
      pictures
    };
  };

  return (
    <div className="App" data-testid="app-container">
      <form onSubmit={getPokemon} data-testid="form">
        <input
          type="number"
          data-testid="input"
          required
          value={pokeNumber}
          onChange={e => setPokeNumber(e.target.value)}
          placeholder="Enter a number"
        />
        <button type="submit" data-testid="button" disabled={message === 'Loading...'}>Submit</button>
      </form>
      {!!pokemon.name ?
        <div className="poke-infos-container">
          <header>
            <span data-testid="pokemon-name">{pokemon.name}</span>
          </header>
          <main>
            {pokemon.pictures.map(({ name, src }) => (
              <img key={name} src={src} alt={name} />
            ))}
          </main>
        </div>
        :
        <h1 className="app-message" data-testid="message">{message}</h1>
      }
    </div>
  );
};

export default App;