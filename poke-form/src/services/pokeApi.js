import axios from 'axios';

const config = {
  endpoint: "https://pokeapi.co/api/v2/",
};

const app = axios.create({ baseURL: config.endpoint });

export default app;