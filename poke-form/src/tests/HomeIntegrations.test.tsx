import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent, act, waitFor } from '@testing-library/react';
import { mocked } from 'ts-jest/utils';

import { getPokemonByNumber } from '../repositories/pokemon';

import Home from '../pages/Home';

jest.mock('../repositories/pokemon')


describe("Principal component - integrations", () => {
  act(() => {
    it('success to get pokemon', async () => {
      const { getByTestId, findByTestId } = render(<Home />);
      const getPokemonByNumberMocked = mocked(getPokemonByNumber)

      getPokemonByNumberMocked.mockResolvedValueOnce({
        status: 200,
        data: {
          name: "bulbasaur",
          sprites: {
            back_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png",
            back_female: null,
            back_shiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/1.png",
            back_shiny_female: null,
            front_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
            front_female: null,
            front_shiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png",
            front_shiny_female: null
          }
        }
      } as any)

      const input = getByTestId('input') as HTMLInputElement;
      const form = getByTestId('form');

      fireEvent.change(input, { target: { value: '1' } });
      await waitFor(() => {
        fireEvent.submit(form)
      })

      expect(await findByTestId('pokemon-name')).toBeInTheDocument()
    })

    it('failure to get pokemon', async () => {
      const { getByTestId, findByTestId } = render(<Home />);
      const input = getByTestId('input') as HTMLInputElement;
      const form = getByTestId('form');

      fireEvent.change(input, { target: { value: '99999' } });
      await waitFor(() => {
        fireEvent.submit(form)
      })

      expect(await findByTestId('message')).toBeInTheDocument()
    })
  })
});