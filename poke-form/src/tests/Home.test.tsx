import { render, fireEvent } from '@testing-library/react';
import Home from '../pages/Home';

const setup = () => {
  const utils = render(<Home />);
  const input = utils.getByTestId('input') as HTMLInputElement;
  const button = utils.getByRole('button') as HTMLButtonElement;
  return {
    input,
    button,
    ...utils,
  };
};

describe('Princial component', () => {
  describe('When access the app', () => {
    it('Input to be enabled', () => {
      const { input } = setup();
      expect(input.disabled).toBe(false);
    });

    it('Button to be enabled', () => {
      const { button } = setup();
      expect(button.disabled).toBe(false);
    });

    it("renders correctly when pokemon is not set", () => {
      const { queryByText } = setup();
      expect(queryByText('Search for pokemon with a number :)')).toBeTruthy()
    });
  });

  describe('When the form is being filled out', () => {
    it('should not allow letters to be inputted', () => {
      const { input } = setup();
      fireEvent.change(input, { target: { value: 'Good Day' } });
      expect(input.value).toBe('');
    });

    it('should allow numbers to be inputted', async () => {
      const { input } = setup();
      fireEvent.change(input, { target: { value: '123' } });

      expect(input.value).toBe('123');
    });
  });
});
