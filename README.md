# Desafio Frontend

## Instalação

### `git clone https://gitlab.com/arturpdev/frontend-challenge-1.git`

Clona o projeto.

No diretório do projeto (**poke-form**), execute `yarn` (ou `npm install`)

## Scripts Disponíveis

No diretório do projeto, você pode executar:

### `yarn start` ou `npm run start`

Executa o aplicativo no modo de desenvolvimento.\
Abra [http://localhost:3000](http://localhost:3000) para visualizá-lo no navegador.

A página será recarregada se você fizer edições.\
Você também verá quaisquer erros de lint no console.

### `yarn test` ou `npm run test`

Inicia o executor de teste no modo de observação interativo.